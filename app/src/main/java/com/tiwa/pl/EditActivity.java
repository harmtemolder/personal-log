package com.tiwa.pl;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class EditActivity extends AppCompatActivity {

    private LogRepository logRepository;

    private LogEntry entry;

    private EditText editText;

    private boolean createNew;

    private Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        Toolbar toolbar = findViewById(R.id.toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveText();
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        res = getResources();

        logRepository = LogRepositoryFactory.createLogRepository(getApplication());
        String entryName = getIntent().getStringExtra(Constants.ENTRY);
        editText = findViewById(R.id.editText);
        if (entryName == null) {
            createNew = true;
            entry = logRepository.getTemplate();
            getSupportActionBar().setTitle(R.string.edit_activity_title_new);
        } else {
            createNew = false;
            entry = logRepository.getEntryByName(entryName);
            checkEmptyTemplateEdit(entry);
            getSupportActionBar().setTitle(String.format(
                    res.getString(R.string.edit_activity_title_edit), entry.toString()));
        }

        editText.setText(entry.getText());
    }

    private void checkEmptyTemplateEdit(LogEntry entry) {
        if (entry.getName().contentEquals(Constants.TEMPLATE_FILENAME) && entry.getText().isEmpty()) {
            entry.setText(getString(R.string.template_sample));
        }
    }

    private void saveText() {
        if (createNew) {
            entry = logRepository.createEntry();
        }
        entry.setText(editText.getText().toString());
        logRepository.updateEntry(entry);
        finish();
    }
}
