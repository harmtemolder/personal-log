package com.tiwa.pl;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Environment;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import java.io.File;
import java.io.IOException;

class LogRepositoryFactory {

    private static LogRepository logRepository;

    private static Boolean toggleCustomPath;

    private static String path;

    static LogRepository createLogRepository(Application app) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
        boolean currentToggleCustomPath = sharedPreferences.getBoolean("toggleCustomPath", false);
        String currentPath = sharedPreferences.getString("path", "");


        if (logRepository == null || !currentPath.contentEquals(path) || currentToggleCustomPath != toggleCustomPath) {
            toggleCustomPath = currentToggleCustomPath;
            path = currentPath;
            String folderPath;
            if (toggleCustomPath && path != null && !path.isEmpty()) {
                if (path.charAt(0) != '/') {
                    folderPath = "/" + path;
                } else {
                    folderPath = path;
                }
                if (folderPath.charAt(folderPath.length() - 1) == '/') {
                    folderPath = folderPath.substring(0, folderPath.length() - 1);
                }

                File externalStorageDirectory = Environment.getExternalStorageDirectory();
                folderPath = externalStorageDirectory.getAbsolutePath() + folderPath;

                try {
                    logRepository = new LogRepositoryImpl(folderPath);
                    return logRepository;
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(app.getApplicationContext(), app.getText(R.string.logRepository_invalidPath_toast),
                            Toast.LENGTH_LONG).show();
                }
            }
            File dataDirectory = app.getApplicationContext().getFilesDir();
            folderPath = dataDirectory.getAbsolutePath();
            try {
                logRepository = new LogRepositoryImpl(folderPath);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return logRepository;
    }
}

