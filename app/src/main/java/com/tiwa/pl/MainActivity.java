package com.tiwa.pl;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final Calendar calendar = Calendar.getInstance();

    private LogRepository logRepository;

    private TextView entryContent;

    private MenuItem prevButton;

    private MenuItem nextButton;

    private List<LogEntry> logEntryList;

    private Resources res;

    private Integer logIndex;


    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            calendar.set(Calendar.HOUR, 0);
            calendar.set(Calendar.MINUTE, 0);
            Date choosenDate = calendar.getTime();
            for (int i = 0; ; i++) {
                if (i == logEntryList.size()) {
                    logIndex = i - 1;
                    break;
                }
                Date entryDate = logEntryList.get(i).getDate();
                if (choosenDate.before(entryDate)) {
                    logIndex = i;
                    break;
                }
            }
            render();
        }
    };

    DialogInterface.OnClickListener deleteDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    deleteEntry();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    break;
            }
        }
    };

    private void initApp() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean firstRun = prefs.getBoolean(Constants.FIRST_RUNT, true);
        if (firstRun) {
            goToSettings();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initApp();
        res = getResources();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        entryContent = findViewById(R.id.contentText);
        final ScrollView scrollView = findViewById(R.id.mainScrollview);
        scrollView.setOnTouchListener(new OnGestureListener(this) {
            @Override
            public void onSwipeRight() {
                lastEntry();
            }

            @Override
            public void onSwipeLeft() {
                nextEntry();
            }

            @Override
            public void longPress() {
                editEntry(logEntryList.get(logIndex).getName());
            }

            @Override
            public void doubleTap() {
                showDatePicker();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, EditActivity.class);
                MainActivity.this.startActivity(myIntent);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        initLogRepository();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        prevButton = menu.getItem(0);
        nextButton = menu.getItem(1);
        render();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.settings || !logEntryList.isEmpty()) {

            switch (id) {
                case R.id.prevEntry:
                    lastEntry();
                    break;
                case R.id.nextEntry:
                    nextEntry();
                    break;
                case R.id.delete:
                    showDeleteCurrentEntry();
                    break;
                case R.id.edit:
                    editEntry(logEntryList.get(logIndex).getName());
                    break;
                case R.id.share:
                    share();
                    break;
                case R.id.goToDate:
                    showDatePicker();
                    break;
                case R.id.settings:
                    goToSettings();
                    break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDatePicker() {
        if (logEntryList.isEmpty()) {
            return;
        }
        new DatePickerDialog(this, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    private void share() {
        if (logEntryList.isEmpty()) {
            return;
        }
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, logEntryList.get(logIndex).toString());
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, logEntryList.get(logIndex).getText());
        startActivity(Intent.createChooser(sharingIntent, String.format(
                res.getString(R.string.main_dialog_share), logEntryList.get(logIndex).toString())));
    }


    private void goToSettings() {
        Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
        MainActivity.this.startActivity(myIntent);
    }

    private void editEntry(String entryName) {
        if (logEntryList.isEmpty()) {
            return;
        }
        Intent myIntent = new Intent(MainActivity.this, EditActivity.class);
        myIntent.putExtra(Constants.ENTRY, entryName);
        MainActivity.this.startActivity(myIntent);
    }

    private void deleteEntry() {
        if (logEntryList.isEmpty()) {
            return;
        }
        logRepository.deleteEntrys(logEntryList.get(logIndex));
        logIndex--;
        loadEntrys();
        render();
    }

    private void showDeleteCurrentEntry() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(String.format(res.getString(R.string.main_dialog_delete_text),
                logEntryList.get(logIndex)))
                .setPositiveButton(R.string.main_dialog_delete_yes, deleteDialogClickListener)
                .setNegativeButton(R.string.main_dialog_delete_no, deleteDialogClickListener)
                .show();
    }

    private void nextEntry() {
        if (logIndex < logEntryList.size() - 1) {
            logIndex++;
            render();
        }
    }

    private void lastEntry() {
        if (logIndex > 0) {
            logIndex--;
            render();
        }
    }

    private void initLogRepository() {
        logRepository = LogRepositoryFactory.createLogRepository(getApplication());
        loadEntrys();
        setIndexLast();
        render();
    }

    private void setIndexLast() {
        logIndex = logEntryList.size() - 1;
    }

    private void loadEntrys() {
        logEntryList = logRepository.listEntrys();
    }


    private void render() {
        toggleShowMenuButton();
        if (!logEntryList.isEmpty()) {
            getSupportActionBar().setTitle(logEntryList.get(logIndex).toString());
            entryContent.setText(logEntryList.get(logIndex).getText());
            TextViewStyler textViewStyler = new TextViewStyler();
            textViewStyler.styleTextView(entryContent);
        }
    }

    private void toggleShowMenuButton() {
        if (nextButton != null && prevButton != null) {
            nextButton.setEnabled(true);
            prevButton.setEnabled(true);
            nextButton.setIcon(R.drawable.ic_next);
            prevButton.setIcon(R.drawable.ic_prev);
            if (logIndex == logEntryList.size() - 1) {
                nextButton.setEnabled(false);
                nextButton.setIcon(R.drawable.ic_empty);
            }
            if (logIndex <= 0) {
                prevButton.setEnabled(false);
                prevButton.setIcon(R.drawable.ic_empty);
            }
        }
    }
}
