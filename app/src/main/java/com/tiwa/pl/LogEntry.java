package com.tiwa.pl;

import androidx.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.tiwa.pl.Constants.DATE_INPUT_PATTERN;
import static com.tiwa.pl.Constants.DATE_OUTPUT_PATTERM;

class LogEntry implements Comparable<LogEntry> {

    private final String name;

    private String text;

    private Date date;

    LogEntry(String name, String text) {
        this.name = name;
        this.text = text;
        setDate(name);
    }

    LogEntry(String name) {
        this.name = name;
        setDate(name);
        this.text = "";
    }

    Date getDate() {
        return date;
    }

    String getName() {
        return name;
    }

    String getText() {
        return text;
    }

    private void setDate(String name) {
        if (isNotDateName()) {
            this.date = null;
        } else {
            SimpleDateFormat simpleDateFormatInput = new SimpleDateFormat(DATE_INPUT_PATTERN, Locale.getDefault());
            try {
                this.date = simpleDateFormatInput.parse(name.substring(0, 12));
            } catch (ParseException e) {
                this.date = null;
            }
        }

    }

    void setText(String text) {
        this.text = text;
    }

    @Override
    @NonNull
    public String toString() {
        if (date == null) {
            return name;
        }
        SimpleDateFormat simpleDateFormatOutput = new SimpleDateFormat(DATE_OUTPUT_PATTERM, Locale.getDefault());
        return simpleDateFormatOutput.format(date);
    }

    @Override
    public int compareTo(LogEntry o) {
        return getName().compareToIgnoreCase(o.getName());
    }

    private boolean isNotDateName() {
        if (name.length() < 12) {
            return true;
        }
        return !(name.substring(0, 12).matches("-?\\d+"));
    }
}
