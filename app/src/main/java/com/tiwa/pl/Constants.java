package com.tiwa.pl;

public final class Constants {

    public static final String ENTRY = "entry";

    public static final String FIRST_RUNT = "firstRun";

    public static final String DATE_INPUT_PATTERN = "yyyyMMddHHmm";

    public static final String DATE_OUTPUT_PATTERM = "E dd.MM HH:mm ";

    public static final String TEMPLATE_FILENAME = "template.txt";

    public static final int STORAGE_PERMISSION = 1234;
}
