package com.tiwa.pl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.tiwa.pl.Constants.TEMPLATE_FILENAME;

public class LogRepositoryImpl implements LogRepository {


    private final String DIR_PATH;

    LogRepositoryImpl(String DIR_PATH) throws IOException {
        File dir = new File(DIR_PATH);
        if (!dir.exists()) {
            throw new IOException();
        }
        this.DIR_PATH = DIR_PATH;
    }

    @Override
    public LogEntry createEntry() {
        LogEntry newLogEntry = new LogEntry(generateFilename());
        writeToFilesystem(newLogEntry);
        return newLogEntry;
    }

    private LogEntry readTemplateFile() {
        String templateContent = getFileContent(new File(DIR_PATH + "/" + TEMPLATE_FILENAME));
        return new LogEntry(TEMPLATE_FILENAME, templateContent);
    }

    private String generateFilename() {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.getDefault());
        String format = s.format(new Date());
        return format + ".txt";
    }

    private void writeToFilesystem(LogEntry logEntry) {
        try {
            File file = new File(DIR_PATH, logEntry.getName());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(logEntry.getText());
            bufferedWriter.flush();
            bufferedWriter.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            System.err.println(e.toString());
        }
    }

    @Override
    public List<LogEntry> listEntrys() {
        return readAllFiles();
    }

    @Override
    public LogEntry getEntryByName(String name) {
        return new LogEntry(name, getFileContent(new File(DIR_PATH + "/" + name)));
    }

    private List<LogEntry> readAllFiles() {
        List<LogEntry> logEntryList = new ArrayList<>();
        File dir = new File(DIR_PATH);
        File[] allFiles = dir.listFiles();
        if (allFiles != null) {
            for (File file : allFiles) {
                LogEntry entry = new LogEntry(file.getName(), getFileContent(file));
                if (entry.getDate() == null) {
                    continue;
                }
                logEntryList.add(entry);
            }
        }
        Collections.sort(logEntryList);
        return logEntryList;
    }

    private String getFileContent(File file) {
        StringBuilder content = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                content.append(line);
                content.append("\n");
            }
            if (content.length() > 0) {
                content.deleteCharAt(content.length() - 1);
            }
            br.close();
        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return content.toString();
    }

    @Override
    public void updateEntry(LogEntry logEntry) {
        writeToFilesystem(logEntry);
    }

    @Override
    public void deleteEntrys(LogEntry logEntry) {
        new File(DIR_PATH + "/" + logEntry.getName()).delete();
    }

    @Override
    public void updateTemplate(LogEntry logEntry) {
        updateEntry(new LogEntry(TEMPLATE_FILENAME, logEntry.getText()));
    }

    @Override
    public LogEntry getTemplate() {
        return readTemplateFile();
    }
}