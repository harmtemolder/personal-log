package com.tiwa.pl;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class LogRepositoryTest {

    private final String TESTDIR = "./testdir/";

    private LogRepository sut = new LogRepositoryImpl(TESTDIR);

    @Before
    public void before() {
        new File(TESTDIR).mkdir();
    }

    @After
    public void after() {
        File folder = new File(TESTDIR);
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                file.delete();
            }
        }
        folder.delete();
    }


    @Test
    public void createFileAndFillWithDataTest() {
        // prepare
        String content = "thisIsATest";

        // execute
        LogEntry logEntry = sut.createEntry();
        logEntry.setText(content);
        sut.updateEntry(logEntry);

        // verify
        List<LogEntry> logEntries = sut.listEntrys();
        assertEquals(1, logEntries.size());
        assertEquals(logEntries.get(0).getText(), content);
    }

    @Test
    public void createMultipleFile() {
        // prepare
        String content1 = "thisIsATest";
        String content2 = "anotherTest";

        // execute
        LogEntry logEntry1 = sut.createEntry();
        logEntry1.setText(content1);
        sut.updateEntry(logEntry1);
        LogEntry logEntry2 = sut.createEntry();
        logEntry2.setText(content2);
        sut.updateEntry(logEntry2);

        // verify
        List<LogEntry> logEntries = sut.listEntrys();
        assertEquals(2, logEntries.size());
    }

    @Test
    public void deleteFileTest() {
        //prepare
        LogEntry entry = sut.createEntry();

        // execute
        sut.deleteEntrys(entry);

        // verify
        List<LogEntry> logEntries = sut.listEntrys();
        assertEquals(0, logEntries.size());
    }

    @Test
    public void setTemplateTest() {
        //prepare
        LogEntry entry = new LogEntry("template");
        String content = "template text";
        entry.setText(content);

        // execute
        sut.updateTemplate(entry);
        LogEntry entry2 = sut.getTemplate();

        // verify
        assertEquals(entry.getText(), entry2.getText());
    }

}
