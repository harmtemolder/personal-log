Personal Log
------------

You can write personal log entries defined by a custom template.

Each entry is saved in a plaintext file. So they can easily shared via Syncthing or Nextcloud.


[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.tiwa.pl/)